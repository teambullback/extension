var SwitchHandler = function() {
    this.userMode = new UserMode();
    this.builderMode = new BuilderMode();
    this.currentTab = undefined;
}

var UserMode = function() {

}

UserMode.prototype = {
    userStatus: undefined,
    tutorialId: undefined,
    initialURL: undefined,
    loginReq: undefined,
    loginURL: undefined,
    nextSelectList: undefined,
    statusTrigger: undefined,
    elementPathErrorNumber: 0,
    currentTab: undefined,
    userTab: undefined,
    isUserTab: undefined,
    initializeUserMode: function(tutorialId) {
        var self = this;
        self.tutorialId = tutorialId;
        // 1. web이나 extension popup에서 tutorial의 id값만을 전달한다
        // 2. local에서 tutorial의 id값을 가지고 서버와의 통신을 통해 initialURL, loginReq, loginURL의 3개 값을 받아온다. (async:false)
        self.getInformation(self.tutorialId);
        // 3. 전달받은 loginReq값을 통해 최초의 userStatus값을 결정해준다. self.setInitialStatus(self.loginReq);
        self.setInitialStatus(self.loginReq);
        // 4. 튜토리얼 실행을 위해 userTab을 생성하거나, 기존에 userTab이 존재할 경우 그 탭을 initialURL값으로 주소를 이동해준다.
        self.setUserTab();
        // 5. 혹시 웹에서 실행했을 경우 init이 주소에 들어가있는 탭을 크롬 브라우저에서 제거해준다. 
        self.removeInitTab();
        // 6. 매번 content script가 삽입될 때마다, 메시지가 전달되면 userStatus에 따라 페이지가 취해야 할 행동을 명령해준다. (request.type === "content_script_started" 부분) 
        // 7. 튜토리얼이 끝나면 userTab과 userStatus를 초기화해준다. (request.type === "user_mode_end_of_tutorial" )
    },
    getInformation: function(tutorialId) {
        var self = this;
        $.ajax({
            url: 'https://webbles.net/api-list/tutorials/' + tutorialId,
            type: "GET",
            async: false
        }).done(function(data) {
            self.loginReq = data.req_login;
            self.loginURL = data.url_login;
            var originalTutorial = data.contents;
            chrome.storage.local.set({
                tutorials: originalTutorial
            });
            var parsedTutorial = JSON.parse(originalTutorial);
            var parsedBubbles = JSON.parse(parsedTutorial.bubbles);
            for (var list in parsedBubbles) {
                if (!parsedBubbles[list].prev) {
                    self.initialURL = parsedBubbles[list].page_url;
                }
            }

        }).fail(function() {
            console.log("server connection failed");
        });
    },
    setInitialStatus: function(loginReq) {
        var self = this;
        switch (loginReq) {
            case true:
                self.userStatus = 0;
                break;
            case false:
                self.userStatus = 1;
                break;
        }
    },
    setUserTab: function() {
        var self = this;
        if (self.userTab === undefined) {
            chrome.tabs.create({
                active: true,
                url: self.initialURL
            }, function(tab) {
                self.userTab = tab.id;
            });
        } else {
            chrome.tabs.update(self.userTab, {
                active: true,
                url: self.initialURL
            }, function(tab) {
                self.userTab = tab.id;
            });
        }
    },
    removeInitTab: function() {
        var self = this;
        var matchingInitTab;
        chrome.tabs.query({
            currentWindow: true,
            url: "*://webbles.net/tutorials*init/",
        }, function(tabs) {
            if (tabs.length >= 1) {
                matchingInitTab = tabs[0].id;
                chrome.tabs.remove(matchingInitTab);
            }
        });
    },
    endTutorial: function() {
        var self = this;
        self.userStatus = 3;
    },
    endUserMode: function() {
        var self = this;
        self.userStatus = undefined;
        self.tutorialId = undefined;
        self.initialURL = undefined;
        self.loginReq = undefined;
        self.loginURL = undefined;
        self.nextSelectList = undefined;
        self.statusTrigger = undefined;
        self.elementPathErrorNumber = 0;
        self.userTab = undefined;
        self.isUserTab = undefined;
    }
}

var BuilderMode = function() {

}

BuilderMode.prototype = {
    builderStatus: undefined,
    tutorialId: undefined,
    currentTab: undefined,
    builderTab: undefined,
    isBuilderTab: undefined,
    initializeBuilderMode: function() {
        var self = this;
        self.builderTab = self.currentTab;
        console.log(self.builderTab);
        chrome.tabs.sendMessage(self.builderTab, {
            type: "initialize_builder_mode"
        }, function(response) {});
    }, 
    setupTutorialId: function(id) {
        var self = this;
        self.tutorialId = id;
    }
}

// 생성자를 통해 SwitchHandler 객체를 선언한다
var SwitchHandler = new SwitchHandler();

chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        //*** UserMode ***//
        if (request.type === "content_script_started") {
            if (sender.tab.id === SwitchHandler.userMode.userTab) {

            }
            switch (SwitchHandler.userMode.userStatus) {
                case 0:
                    chrome.tabs.sendMessage(SwitchHandler.userMode.userTab, {
                        type: "generate_login_modal",
                        data: SwitchHandler.userMode.loginURL
                    }, function(response) {});
                    SwitchHandler.userMode.userStatus = 1;
                    break;
                case 1:
                    chrome.tabs.sendMessage(SwitchHandler.userMode.userTab, {
                        type: "initialize_user_mode",
                        data_1: SwitchHandler.userMode.tutorialId
                    }, function(response) {});
                    SwitchHandler.userMode.userStatus = 2;
                    break;
                case 2:
                    chrome.tabs.sendMessage(SwitchHandler.userMode.userTab, {
                        type: "reload_user_mode",
                        data_1: SwitchHandler.userMode.tutorialId,
                        data_2: SwitchHandler.userMode.nextSelectList,
                        data_3: SwitchHandler.userMode.statusTrigger
                    }, function(response) {});
                    break;
                case 3:
                    chrome.tabs.sendMessage(SwitchHandler.userMode.userTab, {
                        type: "generate_ending_modal",
                        data: SwitchHandler.userMode.tutorialId
                    }, function(response) {});
                    break;
            }
        } else if (request.type === "element_not_found") {
            SwitchHandler.userMode.elementPathErrorNumber++;
            if (SwitchHandler.userMode.elementPathErrorNumber > 50) {
                chrome.tabs.sendMessage(SwitchHandler.userMode.userTab, {
                    type: "alert_message",
                }, function(response) {});
                chrome.runtime.reload();
            }
            window.setTimeout(function() {
                chrome.tabs.sendMessage(SwitchHandler.userMode.userTab, {
                    type: "try_finding_element_path",
                }, function(response) {});
            }, 100);
        } else if (request.type === "user_mode_end_of_tutorial") {
            SwitchHandler.userMode.endTutorial();
        } else if (request.type === "move_to_login_page") {
            SwitchHandler.userMode.userStatus = undefined;
            chrome.tabs.create({
                active: true,
                url: request.data
            }, function(tab) {
                alert("이 페이지에서 로그인 이후에 시작해주세요!");
            });
        } else if (request.type === "initialize_user_mode_from_modal") {
            SwitchHandler.userMode.initializeUserMode(request.data);
            SwitchHandler.userMode.userStatus = 1;
        }
        //*** BuilderMode ***//
        else if (request.type === "builder_tutorial_id_established") {
            SwitchHandler.builderMode.setupTutorialId(request.data);
        }
    }
);

chrome.runtime.onConnect.addListener(
    function(port) {
        port.onMessage.addListener(function(msg) {
            //*** UserMode ***//
            if (msg.type === "initialize_user_mode") {
                SwitchHandler.userMode.initializeUserMode(msg.data_2);
            } else if (msg.type === "user_mode_initialized_from_web") {
                SwitchHandler.userMode.initializeUserMode(msg.data);
            } else if (msg.type === "next_bubble") {
                SwitchHandler.userMode.nextSelectList = msg.data_1;
                SwitchHandler.userMode.statusTrigger = msg.data_3;
            } else if (msg.type === "change_focused_bubble") {
                var movingURL = msg.data_1;
                SwitchHandler.userMode.nextSelectList = msg.data_2;
                SwitchHandler.userMode.statusTrigger = msg.data_3;
                chrome.tabs.update({
                    url: movingURL
                }, function() {});
            } else if (msg.type === "exit_user_mode") {
                // 모달을 내리면서 유저모드 종료
                console.log("exit_user_mode!");
                SwitchHandler.userMode.endUserMode();
            } else if (msg.type === "exit_user_playmode") {
                // 탭을 업데이트 시키면서 유저모드 종료 
                chrome.tabs.update(SwitchHandler.userMode.userTab, {
                    url: msg.data
                }, function(tab) {
                    if (tab.status === "loading") {
                        SwitchHandler.userMode.endUserMode();
                    }
                    alert("위블즈가 종료되었습니다. 사용에 감사드립니다.");
                });
            }
            // 아래의 3개 메시지 이벤트들은 엔딩모달에서 background의 기능을 사용하여 페이지를 열기 위한 행동들을 수행함 
            else if (msg.type === "open_webbles_from_ending_modal") {
                chrome.tabs.create({
                    active: true,
                    url: "https://webbles.net"
                }, function(tab) {});
            } else if (msg.type === "open_tutorial_page_from_ending_modal") {
                var moving_url = "https://webbles.net/tutorials/" + msg.data;
                chrome.tabs.create({
                    active: true,
                    url: moving_url
                }, function(tab) {});
            } else if (msg.type === "share_via_webbles_from_ending_modal") {
                var moving_url = "https://webbles.net/tutorials/" + msg.data_2 + "/?shareRequest=" + msg.data_1;
                chrome.tabs.create({
                    active: true,
                    url: moving_url
                }, function(tab) {});
            }
            //*** BuilderMode ***//
            else if (msg.type === "initialize_builder_mode") {
                SwitchHandler.builderMode.initializeBuilderMode();
            }
        });
    }
);

chrome.tabs.onReplaced.addListener(function(addedTabId, removedTabId) {
    if (removedTabId === SwitchHandler.userMode.userTab) {
        alert("위블즈가 종료되었습니다. 사용에 감사드립니다.");
        SwitchHandler.userMode.endUserMode();
        chrome.runtime.reload();
    }
});

chrome.tabs.onRemoved.addListener(function(tabId, removeInfo) {
    if (tabId === SwitchHandler.userMode.userTab) {
        SwitchHandler.userMode.endUserMode();
        alert("위블즈가 종료되었습니다. 사용에 감사드립니다.");
    }
});

chrome.runtime.onInstalled.addListener(function(details) {
    if (details.reason === "install") {
        chrome.tabs.query({
            url: "*://webbles.net/*"
        }, function(tabs) {
            // 위블즈 홈페이지를 가리키고 있는 모든 탭을 새로고침한다. 
            tabs.forEach(function(value, index, array) {
                chrome.tabs.update(tabs[index].id, {
                    url: tabs[index].url
                }, function(tab) {
                    alert("설치가 완료되었습니다. 이제 이 창에서부터 시작해주시면 됩니다!");
                });
            });
        });
    }
});

chrome.tabs.onActivated.addListener(function(activeInfo) {
    SwitchHandler.userMode.currentTab = activeInfo.tabId;
    SwitchHandler.builderMode.currentTab = activeInfo.tabId;
});